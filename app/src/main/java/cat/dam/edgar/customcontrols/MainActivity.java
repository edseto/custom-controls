package cat.dam.edgar.customcontrols;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    MediaPlayer song;
    LinearLayout ll_top;
    LinearLayout ll_bottom;
    SwitchCompat sw_vista;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ll_top = (LinearLayout) findViewById(R.id.ll_top);
        ll_bottom = (LinearLayout) findViewById(R.id.ll_bottom);

        sw_vista = (SwitchCompat) findViewById(R.id.sw_vista);
        if(sw_vista.isChecked()) sw_vista.setText(R.string.sw_on);
        else sw_vista.setText(R.string.sw_off);

        setSwitchListener();
        setButtonsListener();
    }

    public void setSwitchListener()
    {
        sw_vista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sw_vista.isChecked()){
                    setHorizontal();
                    sw_vista.setText(R.string.sw_on);
                }
                else{
                    setVertical();
                    sw_vista.setText(R.string.sw_off);
                }
            }
        });
    }

    public void setButtonsListener()
    {
        ArrayList<ToggleButton> buttons = getButtons();

        for (ToggleButton btn: buttons) {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(btn.isChecked()) {
                        if(song != null) {
                            song.stop();
                            song = null;
                        }
                        uncheckOthers(buttons, btn);
                        music(btn);
                    } else {
                        song.stop();
                        song = null;
                    }
                }
            });
        }
    }

    public void uncheckOthers(ArrayList<ToggleButton> buttons, ToggleButton btn)
    {
        for (ToggleButton tbutton: buttons) {
            if (tbutton.getId() != btn.getId()) tbutton.setChecked(false);
        }
    }

    public ArrayList<ToggleButton> getButtons()
    {
        ArrayList<ToggleButton> buttons = new ArrayList<>();
        ToggleButton tbtn;
        int buttonsTop, buttonsBottom;

        buttonsTop = ll_top.getChildCount();
        buttonsBottom = ll_bottom.getChildCount();

        for (int i = 0; i < buttonsTop; i++) {
            tbtn = (ToggleButton) ll_top.getChildAt(i);
            buttons.add(tbtn);
        }

        for (int i = 0; i < buttonsBottom; i++) {
            tbtn = (ToggleButton) ll_bottom.getChildAt(i);
            buttons.add(tbtn);
        }

        return buttons;
    }

    public void music(ToggleButton v)
    {
        String name = v.getContentDescription().toString().toLowerCase();
        int resource;

        name = name.replaceAll(" ", "_");
        resource = getResources().getIdentifier(name, "raw", this.getPackageName());

        song = MediaPlayer.create(getApplicationContext(), resource);
        song.start();
    }

    protected void setHorizontal()
    {
        ll_top.setOrientation(LinearLayout.HORIZONTAL);
        ll_bottom.setOrientation(LinearLayout.HORIZONTAL);

        changeButtonHeight(450);
        ConstraintLayout main = (ConstraintLayout) findViewById(R.id.cl_main);
        ConstraintSet set = new ConstraintSet();

        set.clone(main);
        set.setVerticalBias(ll_bottom.getId(), (float) 0.045);
        set.applyTo(main);
    }

    protected void setVertical()
    {
        ll_top.setOrientation(LinearLayout.VERTICAL);
        ll_bottom.setOrientation(LinearLayout.VERTICAL);

        changeButtonHeight(265);
        ConstraintLayout main = (ConstraintLayout) findViewById(R.id.cl_main);
        ConstraintSet set = new ConstraintSet();

        set.clone(main);
        set.setVerticalBias(ll_bottom.getId(), (float) 0.0);
        set.applyTo(main);
    }

    protected void changeButtonHeight(int height)
    {
        ArrayList<ToggleButton> buttons = getButtons();

        for (ToggleButton tbtn: buttons) {
            tbtn.getLayoutParams().height = height;
        }
    }
}